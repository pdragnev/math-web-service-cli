package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"log"
	"net/http"
)

// evaluateCmd represents the evaluate command

var (
	evaluateExpression string
	evaluateCmd        = &cobra.Command{
		Use:   "evaluate",
		Short: "Parse and evaluate a problem expression returning the answer as an integer",
		Long: `Parse and evaluate a problem expression returning the answer as an integer. 
Since these are verbal word problems, evaluate the expression from
left-to-right, ignoring the typical order of operations. For example:
  "What is 5?" should return 5.
  What is 3 plus 2 multiplied by 3? should return 15
`,
		Run: func(cmd *cobra.Command, args []string) {
			getEvaluateResp(evaluateExpression)
		},
	}
)

func init() {
	rootCmd.AddCommand(evaluateCmd)
	evaluateCmd.Flags().StringVarP(&evaluateExpression, "expression", "e", "", "expression input")
	evaluateCmd.MarkFlagRequired("expression")

}

func getEvaluateResp(expression string) {

	jsonData, err := json.Marshal(map[string]string{"expression": expression})

	if err != nil {
		fmt.Println(err)
		log.Fatal(err)
	}

	resp, err := http.Post("http://localhost:8000/evaluate", "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		fmt.Println(err)
		return
	}

	var evalResp map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&evalResp)
	marshal, _ := json.Marshal(evalResp)
	fmt.Println(string(marshal))
}
