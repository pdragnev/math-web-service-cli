package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/spf13/cobra"
)

// validateCmd represents the validate command
var (
	validateExpression string
	validateCmd        = &cobra.Command{
		Use:   "validate",
		Short: "Validate an expression, return if valid or not.",
		Long: `Validate an expression, return if valid or not.
For example: "What is 1 plus plus 2?" should return:
{
"valid":false,
"reason":"Invalid syntax."
}
`,
		Run: func(cmd *cobra.Command, args []string) {
			getValidateResp(validateExpression)
		},
	}
)

func init() {
	rootCmd.AddCommand(validateCmd)
	validateCmd.Flags().StringVarP(&validateExpression, "expression", "e", "", "expression input")
	validateCmd.MarkFlagRequired("expression")

}

func getValidateResp(expression string) {

	jsonData, err := json.Marshal(map[string]string{"expression": expression})

	if err != nil {
		fmt.Println(err)
		log.Fatal(err)
	}

	resp, err := http.Post("http://localhost:8000/validate", "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		fmt.Println(err)
		return
	}

	var validResp map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&validResp)
	marshal, _ := json.Marshal(validResp)
	fmt.Println(string(marshal))
}
