package cmd

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/spf13/cobra"
)

// errorsCmd represents the errors command
var errorsCmd = &cobra.Command{
	Use:   "errors",
	Short: "Return all errors occurred with their frequency",
	Long: `Return all errors occurred with their frequency (e.g. if the same unsupported
expression is submitted many times), URL of the endpoint on which they have
occurred (i.e. /evaluate or /validate) and the type of the error (e.g. Unsupported
operations).`,
	Run: func(cmd *cobra.Command, args []string) {
		getErrorsResp()
	},
}

func init() {
	rootCmd.AddCommand(errorsCmd)
}

func getErrorsResp() {
	resp, err := http.Get("http://localhost:8000/errors")
	if err != nil {
		fmt.Println(err)
		return
	}

	var errorsResp map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&errorsResp)
	marshal, _ := json.Marshal(errorsResp)
	fmt.Println(string(marshal))
}
