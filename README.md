# Math word problems web service cli

CLI client for [Math web service](https://gitlab.com/pdragnev/math-web-service)

## Installation

To install go to a folder where you want to set up the project and clone the repo

```bash
git clone https://gitlab.com/pdragnev/math-web-service-cli.git
```

Open to cloned directory and in a terminal run

```
go get
go build
```

This will generate the binary for the app

## Usage
Run this command in the main directory of the project
```bash
go run main.go evaluate --expression "<a simple math problem>"
go run main.go validate --expression "<a simple math problem>"
go run main.go errors
```
